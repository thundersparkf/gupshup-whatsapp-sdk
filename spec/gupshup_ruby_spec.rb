require 'uuid'
require 'logger'

RSpec.describe Gupshup do
  before do
    @logger = Logger.new($stdout)
    is_variables_set = validate_env_variables
    unless is_variables_set
      exit 1
    end
    @app = ENV["GUPSHUP_APP"]
    @apikey = ENV["GUPSHUP_APP_APIKEY"]
    @phone = '917834811114'
    @version  = ENV["GUPSHUP_API_VERSION"]
    @destination = ENV["GUPSHUP_DESTINATION_PHONE"]

  end
  it 'has a version number' do
    expect(GupshupRuby::VERSION).not_to be nil
  end
  it 'Sends List' do
    payload = { :type => 'list',:title => 'Rspec tests', :body => 'This is rspec testing body',
                :globalButtons => [{ :type => 'text', :title => 'Test List' }],
                :items  =>[ { "title": "first Section", "subtitle": "first Subtitle", "options": [ { "type": "text", "title": "section 1 row 1", "description": "first row of first section description", "postbackText": "section 1 row 1 postback payload" }, { "type": "text", "title": "section 1 row 2", "description": "second row of first section description", "postbackText": "section 1 row 2 postback payload" }, { "type": "text", "title": "section 1 row 3", "description": "third row of first section description", "postbackText": "section 1 row 3 postback payload" } ] }, { "title": "second section", "subtitle": "second Subtitle", "options": [ { "type": "text", "title": "section 2 row 1", "description": "first row of second section description", "postbackText": "section 1 row 3 postback payload" } ] } ]}

    @logger.info("app: %s, phone: %s, version: %s, destination: %s" % [@app, @apikey, @version, @destination])
    g = Gupshup::REST::OutboundMessage.new(@app, @apikey, @phone, @version)
    req = g.send(@destination, payload)
    @logger.info(req.inspect)
    expect(req.status_code).to be 202
  end

  it 'Sends sticks' do
    payload = { :type => 'sticker', :url => 'https://blog.jiayu.co/2019/07/telegram-animated-stickers/sticker.webp'}

    @logger.info("app: %s, phone: %s, version: %s, destination: %s" % [@app, @apikey, @version, @destination])
    g = Gupshup::REST::OutboundMessage.new(@app, @apikey, @phone, @version)
    req = g.send(@destination, payload)
    @logger.info(req.inspect)
    expect(req.status_code).to be 202
  end

  it 'Sends Quick Reply' do
    payload = {
      :type => "quick_reply",
      :content =>  {"type":"text", "text": "Fave book?", "caption": "Malatesta edition"},
      # :options => [{"type":"text","title":"Anarchy","postbackText":"Good one"},{"type":"text","title":"Conversations in coffee shop"},{"type":"text","title":"Third"}]}
      :options => [{ "title": "Anarchy"}, {"title": "Anarchists vs plague"}]}

    @logger.info("app: %s, phone: %s, version: %s, destination: %s" % [@app, @apikey, @version, @destination])
    g = Gupshup::REST::OutboundMessage.new(@app, @apikey, @phone, @version)
    @logger.info("app: %s, phone: %s, version: %s" % [@app, @apikey, @version])
    req = g.send(@destination, payload)
    @logger.info(req.inspect)
    expect(req.status_code).to be 202
  end

  it 'Sends texts' do
    payload = {
      :isHSM => false,
      :type => "text",
      :text => 'Test message'
    }

    @logger.info("app: %s, phone: %s, version: %s, destination: %s" % [@app, @apikey, @version, @destination])
    g = Gupshup::REST::OutboundMessage.new(@app, @apikey, @phone, @version)
    req = g.send(@destination, payload)

    expect(req.status_code).to be 202
  end

  it 'Sends images' do
    payload = {
      :type => "image",
      :originalUrl => 'https://www.buildquickbots.com/whatsapp/media/sample/jpg/sample01.jpg',
      :previewUrl => 'https://www.buildquickbots.com/whatsapp/media/sample/jpg/sample02.jpg',
      :caption => 'Test Image'
    }

    @logger.info("app: %s, phone: %s, version: %s, destination: %s" % [@app, @apikey, @version, @destination])
    g = Gupshup::REST::OutboundMessage.new(@app, @apikey, @phone, @version)
    req = g.send(@destination, payload)
    expect(req.status_code).to be 202
  end

  it 'Sends videos' do
    payload = {
      :type => "video",
      :url => 'https://www.buildquickbots.com/whatsapp/media/sample/video/sample01.mp4',
      :caption => 'Test Video'
    }

    @logger.info("app: %s, phone: %s, version: %s, destination: %s" % [@app, @apikey, @version, @destination])
    g = Gupshup::REST::OutboundMessage.new(@app, @apikey, @phone, @version)
    req = g.send(@destination, payload)
    expect(req.status_code).to be 202
  end

  it 'Sends files' do
    payload = {
      :type => "file",
      :url => 'https://www.buildquickbots.com/whatsapp/media/sample/pdf/sample01.pdf',
      :filename => 'Test file'
    }

    @logger.info("app: %s, phone: %s, version: %s, destination: %s" % [@app, @apikey, @version, @destination])
    g = Gupshup::REST::OutboundMessage.new(@app, @apikey, @phone, @version)
    req = g.send(@destination, payload)
    expect(req.status_code).to be 202
  end

  it 'Sends audio' do
    payload = {
      :type => "audio",
      :url => 'https://www.buildquickbots.com/whatsapp/media/sample/audio/sample01.mp3'
    }

    @logger.info("app: %s, phone: %s, version: %s, destination: %s" % [@app, @apikey, @version, @destination])
    g = Gupshup::REST::OutboundMessage.new(@app, @apikey, @phone, @version)
    req = g.send(@destination, payload)
    expect(req.status_code).to be 202
  end

  it 'Sends location' do
    payload = {
      :type => "location",
      :longitude => 43.43,
      :latitude => 33.34,
      :name => "Test name",
      :address => "Test address"
    }

    @logger.info("app: %s, phone: %s, version: %s, destination: %s" % [@app, @apikey, @version, @destination])
    g = Gupshup::REST::OutboundMessage.new(@app, @apikey, @phone, @version)
    req = g.send(@destination, payload)
    expect(req.status_code).to be 202
  end

  #it 'Optin list of users' do
  #  g = Gupshup::REST::UserEvents.new(@app, @apikey, @phone)
  #  req = g.send()
  #  expect(req.status_code).to be 200
  # end
end
