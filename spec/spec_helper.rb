require "bundler/setup"
require "gupshup_whatsapp"

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect

  end
end

def validate_env_variables
  logger = Logger.new($stdout)
  app = ENV["GUPSHUP_APP"]
  apikey = ENV["GUPSHUP_APP_APIKEY"]
  version  = ENV["GUPSHUP_API_VERSION"]
  destination = ENV["GUPSHUP_DESTINATION_PHONE"]
  if app == nil
    logger.info("Please set GUPSHUP_APP environment variable")
    return false
  elsif apikey == nil
    logger.info("Please set GUPSHUP_APP_APIKEY environment variable")
    return false
  elsif version == nil
    logger.info("Please set GUPSHUP_API_VERSION environment variable")
    return false
  elsif destination == nil
    logger.info("Please set GUPSHUP_DESTINATION_PHONE environment variable")
    return false
  end
  true
end
