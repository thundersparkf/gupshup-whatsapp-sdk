# CHANGELOG

[1.0.0] - 2021-09-11
Full functionality and all message types supported
## Added
- Support for lists, quick replies and stickers

## Fixed
- Tests with updated sample URLs
