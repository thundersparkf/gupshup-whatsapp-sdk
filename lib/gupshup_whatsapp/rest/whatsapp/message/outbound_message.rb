require_relative '../whatsapp'
module Gupshup
  module REST
class OutboundMessage < Gupshup::REST::WhatsApp
  attr_accessor :phone, :app, :base_uri, :apikey, :version, :content_type, :path, :headers

  def initialize(app, apikey, phone, version = '2')
    super
    @channel = 'whatsapp'
    @source = phone
    @headers = { 'Content-type' => @content_type, 'apikey' => @apikey }
  end

  def send(destination, message_params)
    # Exception handling for parameters to be added
    case message_params[:type]
    when 'text'
      if message_params[:isHSM]
        send_text_message(destination, message_params[:text], is_hsm = true)
      else
        send_text_message(destination, message_params[:text], is_hsm = false)
      end
    when 'sticker'
      send_sticker(destination, message_params[:url])
    when 'image'
      send_image(destination, message_params[:originalUrl], message_params[:previewUrl], message_params[:caption])
    when 'video'
      send_video(destination, message_params[:url], message_params[:caption])
    when 'audio'
      send_audio(destination, message_params[:url])
    when 'file'
      send_file(destination, message_params[:url], message_params[:filename])
    when 'location'
      send_location(destination, message_params[:longitude], message_params[:latitude], message_params[:name], message_params[:address])
    when 'list'
      send_list(destination, message_params[:title], message_params[:body], message_params[:globalButtons], message_params[:items])
    when 'quick_reply'
      send_button(destination, message_params[:content], message_params[:options])
    else
      puts 'Done'
    end
    end

  # Sends a text
  # @param destination [String] destination phone number
  # @param message [String] text message
  # @param is_hsm [Boolean] specifies whether message is a template.
  def send_text_message(destination, message, is_hsm=false)
    payload = {'isHSM' => is_hsm,
               'type' => 'text',
               'text' => message }.to_json
    data = { 'channel' => @channel,
             'destination' => destination.to_s,
             'source' => @source,
             'src.name' => @app,
             'message' => payload }
    r = Gupshup::HTTP::Client.new
    r.request(host = base_uri, port = 443, method = 'POST', url = path, data = data, headers = @headers)
  end


  # Sends an image
  # @param destination [String] destination phone number
  # @param image_url [String] url of the image(publicly available) to send
  # @param preview_url [String] url of the preview image(publicly available) to be displayed before the download of
  # the image
  # @param caption [String] caption text for the image
  def send_image(destination, image_url, preview_url, caption)
    payload = { 'type' => 'image', 'originalUrl' => image_url,
                'previewUrl' => preview_url, 'caption' => caption }.to_json
    data = { 'channel' => @channel, 'destination' => destination.to_s,
             'source' => @source, 'src.name' => @app,
             'message' => payload }
    puts payload
    r = Gupshup::HTTP::Client.new
    r.request(host = base_uri, port = 443, method = 'POST', url = path, data = data, headers = @headers)
  end

  # Sends a Whatsapp Sticker
  # @param destination [String] destination phone number
  # @param url [String] url of the sticker to send
  def send_sticker(destination, url)
    payload = {
      'type': 'sticker',
      'url': url
    }.to_json

    data = { 'channel' => @channel,
             'destination' => destination.to_s,
             'source' => @source,
             'src.name' => @app,
             'message' => payload }
    r = Gupshup::HTTP::Client.new
    r.request(host = base_uri, port = 443, method = 'POST', url = path, data = data, headers = @headers)
  end

  # Sends a video
  # @param destination [String] destination phone number
  # @param url [String] url of the video(publicly available) to send
  # @param caption [String] caption text for the video
  def send_video(destination, url, caption)
    payload = {'url' => url,
               'type' => 'video',
               'caption' => caption }.to_json

    data = { 'channel' => @channel,
             'destination' => destination.to_s,
             'source' => @source,
             'src.name' => @app,
             'message' => payload }
    r = Gupshup::HTTP::Client.new
    r.request(host = base_uri, port = 443, method = 'POST', url = path, data = data, headers = @headers)
  end

  # Sends a file
  # @param destination [String] destination phone number
  # @param url [String] url of the video(publicly available) to send
  # @param filename [String] name of the file to be displayed in the chat
  def send_file(destination, url, filename)
    payload = {'url' => url,
               'type' => 'file',
               'filename' => filename }.to_json

    data = { 'channel' => @channel,
             'destination' => destination.to_s,
             'source' => @source,
             'src.name' => @app,
             'message' => payload }
    r = Gupshup::HTTP::Client.new
    r.request(host = base_uri, port = 443, method = 'POST', url = path, data = data, headers = @headers)
  end

  # Sends an audio
  # @param destination [String] destination phone number
  # @param url [String] url of the audio(publicly available) to send
  def send_audio(destination, url)
    payload = {'url' => url,
               'type' => 'audio' }.to_json

    data = { 'channel' => @channel,
             'destination' => destination.to_s,
             'source' => @source,
             'src.name' => @app,
             'message' => payload }
    r = Gupshup::HTTP::Client.new
    r.request(host = base_uri, port = 443, method = 'POST', url = path, data = data, headers = @headers)
  end

  # Sends a location
  # @param destination [String] destination phone number
  # @param longitude [String] longitude
  # @param latitude [String] latitude
  # @param name [String] name of the location to be displayed in the message
  # @param address [String] address displayed as a caption to the location attachment in the message
  def send_location(destination, longitude, latitude, name, address)
    payload = { "type": 'location',
                "longitude": longitude, "latitude": latitude,
                "name": name, "address": address }.to_json

    data = { 'channel' => @channel,
             'destination' => destination.to_s,
             'source' => @source,
             'src.name' => @app,
             'message' => payload }
    r = Gupshup::HTTP::Client.new
    r.request(host = base_uri, port = 443, method = 'POST', url = path, data = data, headers = @headers)
  end

  # Sends a list
  # @param destination [String] destination phone number
  # @param title [String] longitude
  # @param body [String] latitude
  # @param global_buttons [Array<String, String>] Buttons type(valid value: text) and Title of the button. [{"type": "text", "title": "Button title"}].
  # @param items [Array<String, String>] list items. Refer to https://docs.gupshup.io/docs/whatsapp-message-type-outbound-free-form#lists for format
  def send_list(destination, title, body, global_buttons, items)
    uuid = UUID.new
    payload = { "type": 'list', "title": title, "body": body, "msgid": uuid.generate ,
                "globalButtons": [{ "type": global_buttons[0][:type], "title": global_buttons[0][:title] }],
                "items": items }.to_json
    data = { 'channel' => @channel,
             'destination' => destination.to_s,
             'source' => @source,
             'src.name' => @app,
             'message' => payload }

    r = Gupshup::HTTP::Client.new
    r.request(host = base_uri, port = 443, method = 'POST', url = path, data = data, headers = @headers)
  end

  # Sends a button/quick reply
  # @param destination [String] destination phone number
  # @param content [String] message content
  # @param options [Array<String, String>] list of options with title text and message text.
  def send_button(destination, content, options)
    uuid = UUID.new
    payload = { "type": 'quick_reply', "content": content, "msgid": uuid.generate,
                "options": options}.to_json
    data = { 'channel' => @channel,
             'destination' => destination.to_s,
             'source' => @source,
             'src.name' => @app,
             'message' => payload }
    r = Gupshup::HTTP::Client.new
    r.request(host = base_uri, port = 443, method = 'POST', url = path, data = data, headers = @headers)
  end

end
  end
end
