
require_relative '../whatsapp'

module Gupshup
  module REST
    class UserEvents < Gupshup::REST::WhatsApp
      attr_accessor :app, :base_uri, :apikey, :version, :content_type, :headers

      def initialize(app, apikey, version = '2')
        super
        @headers = { 'apikey' => @apikey }
      end

      def send
        r = Gupshup::HTTP::Client.new
        r = r.request(host = base_uri, port = 443, method = 'GET', url = "/sm/api/v1/users/#{app}", headers = @headers)
        puts r.inspect
        r
      end
    end
  end
end