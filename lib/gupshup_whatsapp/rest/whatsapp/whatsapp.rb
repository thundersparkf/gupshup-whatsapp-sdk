
module Gupshup
  module REST
    class WhatsApp
      attr_reader :content_type, :apikey, :base_uri, :app, :phone, :path

      def initialize(app, apikey, version = '1', phone="917384811114")
        @app = app
        @apikey = apikey
        @version = version
        @content_type = 'application/x-www-form-urlencoded'
        @base_uri = 'https://api.gupshup.io'
        @path = '/sm/api/v1/msg'
        @phone = phone
      end

  end
  end
end

