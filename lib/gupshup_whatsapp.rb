require 'net/http'
require 'net/https'
require 'cgi'
require 'openssl'
require 'base64'
require 'forwardable'
require 'time'
require 'json'
require 'logger'
Dir["#{File.dirname(__FILE__)}/gupshup_whatsapp/http/**/*.rb"].sort.each do |file|
  require file
end
Dir["#{File.dirname(__FILE__)}/gupshup_whatsapp/framework/**/*.rb"].sort.each do |file|
  require file
end
Dir["#{File.dirname(__FILE__)}/gupshup_whatsapp/rest/*.rb"].sort.each do |file|
  require file
end
Dir["#{File.dirname(__FILE__)}/gupshup_whatsapp/rest/**/*.rb"].sort.each do |file|
  require file
end
Dir["#{File.dirname(__FILE__)}/gupshup_whatsapp/rest/**/**/*.rb"].sort.each do |file|
  require file
end
Dir["#{File.dirname(__FILE__)}/gupshup_whatsapp/compatibility/**/*.rb"].sort.each do |file|
  require file
end

module Gupshup
  class Error < StandardError; end
  class WhatsApp
    attr_accessor :app, :apikey
    def initialize(app, apikey, version = '2', phone)
      @app = app
      @apikey = apikey
      @version = version
      @phone = phone
    end
  end
end
