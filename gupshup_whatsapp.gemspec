require_relative 'lib/gupshup_whatsapp/version'

Gem::Specification.new do |spec|
  spec.name          = "gupshup_whatsapp"
  spec.version       = ENV["GEM_VERSION"]
  spec.authors       = ["thundersparkf"]
  spec.email         = ["devagastya0@gmail.com"]

  spec.summary       = "Ruby Wrapper for Gupshup Whatsapp API"
  spec.description   = "This is a simple open source implementation of the Ruby wrapper for Gupshup WhatsApp API. For more instructions, refer to the source code README.md. For message structure, please refer to https://docs.gupshup.io/reference/msg for the structure."
  spec.homepage      = "https://rubygems.org/"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/thundersparkf/gupshup-whatsapp-sdk.git"
  spec.metadata["documentation_uri"] ="https://thundersparkf.gitlab.io/gupshup-whatsapp-sdk/"
  spec.metadata["bug_tracker_uri"] = "https://gitlab.com/thundersparkf/gupshup-whatsapp-sdk/-/issues"
  # spec.metadata["changelog_uri"] = "https://gitlab.com/thundersparkf/gupshup_whatsapp/-/blob/21dc7da6a580d24af1280e7bfb8e22a7e644ddd7/CHANGELOG"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
