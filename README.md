# GupshupRuby

[![Gem Version](https://badge.fury.io/rb/gupshup_whatsapp.svg)](https://badge.fury.io/rb/gupshup_whatsapp)

Gupshup Whatsapp API as a Gem in Ruby for easier integration

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gupshup_whatsapp'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install gupshup_ruby

To test, refer to [Testing](#testing)
## Usage

Use the Gupshup API [docs](https://www.gupshup.io/developer/docs/bot-platform/guide/whatsapp-api-documentation) to construct the payload for a message type.

Refer to gem [documentation](https://thundersparkf.gitlab.io/gupshup-whatsapp-sdk/Gupshup/REST/OutboundMessage.html) for the methods and their required parameters
#### Image payload:
```
payload = {
      :type => "image",
      :originalUrl => 'https://www.buildquickbots.com/whatsapp/media/sample/jpg/sample01.jpg',
      :previewUrl => 'https://www.buildquickbots.com/whatsapp/media/sample/jpg/sample02.jpg',
      :caption => 'Test Image'
    }
```
#### Text Payload

```
payload = {
      :isHSM => false,
      :type => "text",
      :text => 'Test message'
    }
```

#### List Payload

```
payload = { :type => 'list',
			:title => 'Rspec tests',
			:body => 'This is rspec testing body',
            :globalButtons => [{ :type => 'text', :title => 'Test List' }],
            :items  =>[
            	{ "title": "first Section", "subtitle": "first Subtitle", "options": [
            		{ "type": "text", "title": "section 1 row 1", "description": "first row of first section description", "postbackText": "section 1 row 1 postback payload" },
            		{ "type": "text", "title": "section 1 row 2", "description": "second row of first section description", "postbackText": "section 1 row 2 postback payload" },
            		{ "type": "text", "title": "section 1 row 3", "description": "third row of first section description", "postbackText": "section 1 row 3 postback payload" } ]
            	},
            	{
            	  "title": "second section", "subtitle": "second Subtitle", "options": [
            	  	{ "type": "text", "title": "section 2 row 1", "description": "first row of second section description", "postbackText": "section 1 row 3 postback payload" }
            	  ]
            	}
            ]
         }

```

#### Sticker Payload

```
payload = {
      'type': 'sticker',
      'url': url
}
```

#### Videos Payload

```
payload = {
      :type => "video",
      :url => 'https://www.buildquickbots.com/whatsapp/media/sample/video/sample01.mp4',
      :caption => 'Test Video'
    }
```

#### Files Payload

```
payload = {
      :type => "file",
      :url => 'https://www.buildquickbots.com/whatsapp/media/sample/pdf/sample01.pdf',
      :filename => 'Test file'
    }
```

#### Text Payload

```
    payload = {
      :type => "quick_reply",
      :content =>  {"type":"text", "text": "Fave book?", "caption": "Malatesta edition"},
      :options => [{ "title": "Anarchy"}, {"title": "Anarchists vs plague"}]}

```

#### Audio Payload

```
payload = {
      :type => "audio",
      :url => 'https://www.buildquickbots.com/whatsapp/media/sample/audio/sample01.mp3'
    }
```

#### Location Payload

```
payload = {
      :type => "location",
      :longitude => 43.43,
      :latitude => 33.34,
      :name => "Test name",
      :address => "Test address"
    }
```

After this, run the following commands in your code after replacing the credentials and phone number:
```
g = Gupshup::WhatsApp.new(app, apikey, phone_number, version='2')
g.send('9163xxxxxxxx', payload)
```
## <a name="testing"></a>Testing
To run tests, set the following environmental variables:

```shell
export GUPSHUP_DESTINATION_PHONE=
export GUPSHUP_APP=
export GUPSHUP_APP_APIKEY=
export GUPSHUP_API_VERSION=2
```
And the run:
```shell
rspec --format doc
```
## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/thundersp/gupshup_ruby. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/gupshup_ruby/blob/master/CODE_OF_CONDUCT.md).


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the GupshupRuby project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/gupshup_ruby/blob/master/CODE_OF_CONDUCT.md).
